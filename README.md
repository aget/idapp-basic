# idapp basic

idapp-think是基于thinkphp6.1/8.0的核心定制的基础库，内置了模板引擎、多应用、和一些常用类库

## 安装
~~~
composer require idapp/think:dev-master
composer update idapp/think
~~~

## 本地使用
~~~
"require": {
    ...
    "idapp/think": "dev-master"
},
"repositories": [
    {
        "type": "path",
        "url": "../idapp-think"
    }
],
~~~

